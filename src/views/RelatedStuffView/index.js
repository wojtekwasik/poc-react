var React = require('react');
var helpers = require('../../helpers');
var mui = require('material-ui');

module.exports = React.createClass({

  render: function () {
    return (
      <div className='RelatedStuff-view' >
        <h2>Related Stuff</h2>
        <div layout="row">
          <mui.Paper zDepth={1}>
            <h4>Lorem impus</h4><p ph-txt="2s">Tellus integer feugiat scelerisque varius morbi enim nunc, faucibus. Id faucibus nisl tincidunt eget.</p>
          </mui.Paper>
          <mui.Paper zDepth={1}>
            <h4>Lorem impus</h4><p ph-txt="2s">Tellus integer feugiat scelerisque varius morbi enim nunc, faucibus. Id faucibus nisl tincidunt eget.</p>
          </mui.Paper>
          <mui.Paper zDepth={1}>
            <h4>Lorem impus</h4><p ph-txt="2s">Tellus integer feugiat scelerisque varius morbi enim nunc, faucibus. Id faucibus nisl tincidunt eget.</p>
          </mui.Paper>
        </div>
      </div>
    );
  }

});
