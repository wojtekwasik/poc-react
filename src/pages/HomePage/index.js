var React = require('react');
var mui = require('material-ui');
var RelatedStuff = require('../../views/RelatedStuffView');


module.exports = React.createClass({
  render: function () {
    return (
      <div className='home-page'>
        <mui.Input
            hintText="Hint Text" placeholder="Floating Label Text"/>
          <RelatedStuff />
      </div>
    );
  }

});
