var React = require('react');
var mui = require('material-ui');

var List = React.createClass({
    getInitialState: function () {
        return {
            data: [{
                "title": "occaecat culpa laborum",
                "text": "Officia eiusmod ea exercitation sunt pariatur esse ex ipsum laborum proident. " +
                "Elit id cupidatat sunt do. Ad irure anim qui ut consequat laborum officia ut nulla sint amet cillum " +
                "eiusmod fugiat. Ut deserunt anim fugiat qui aliqua laborum ullamco qui dolor ullamco cillum. " +
                "Quis consequat culpa et consectetur dolor aliquip sunt.\r\n"
            },
            {
                "title": "in pariatur ut",
                "text": "Ea dolore nisi esse nisi et magna ex mollit aute magna adipisicing ut. " +
                "Nulla duis sunt exercitation officia. Nulla consectetur culpa anim consectetur " +
                "aliqua esse. Laboris labore dolore elit anim nulla.\r\n"
            },
            {
                "title": "deserunt in non",
                "text": "Aute voluptate irure aliqua ut voluptate ut reprehenderit duis eu dolor " +
                "culpa anim. Labore dolor pariatur laborum consectetur eu ad esse reprehenderit aute " +
                "dolore exercitation et. Proident in voluptate elit ad labore et duis ad minim. Magna " +
                "reprehenderit cupidatat cupidatat amet est elit tempor eu eiusmod minim sit nostrud ullamco.\r\n"
            }]
        };
    },
    render: function () {
        var items = this.state.data.map(function (item, index) {
            return (
                // `key` is a React-specific concept and is not mandatory for the
                // purpose of this tutorial. if you're curious, see more here:
                // http://facebook.github.io/react/docs/multiple-components.html#dynamic-children
                <Comment title={item.title} text={item.text} key={index}></Comment>
            );
        });

        return (
            <div className="list">
                {items}
            </div>
        );
    }
});

var Comment = React.createClass({
    render: function () {
        return (
            <mui.Paper zDepth={1}>
                <div className="comment">
                    <h5 className="commentAuthor">
                        {this.props.title}
                    </h5>
                    <span>{this.props.text}</span>
                </div>
            </mui.Paper>
        );
    }
});

module.exports = List;
